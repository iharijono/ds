# *********************************************************************************************************************
# Owner         :   Indra G. Harijono
# Created       :   Fri, 10/18/2019
# 
# Require       :   Terraform 0.12 or higher
#                   AWS Account: IAM User with access key Id and Key
#                                use:
#                                   AWS_ACCESS_KEY_ID=AKIA3HDCOW2D3HFGSLPG
#                                   AWS_SECRET_ACCESS_KEY=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
# 
# Description   :   main config file for stage 
# DEPLOY A WEBSERVER CLUSTER USING THE WEB MODULE
# *********************************************************************************************************************


# ----------------------------------------------------------------------------------------------------------------------
# REQUIRE A SPECIFIC TERRAFORM VERSION OR HIGHER
# This module has been updated with 0.12 syntax, which means it is no longer compatible with any versions below 0.12.
# ----------------------------------------------------------------------------------------------------------------------

terraform {
  required_version = ">= 0.12"
}

# ------------------------------------------------------------------------------
# CONFIGURE OUR AWS CONNECTION
# ------------------------------------------------------------------------------

provider "aws" {
  # ok, you can choose where the closest is
  region = "us-east-2"
  # To prevent automatic upgrades to new major versions that may contain breaking changes
  version = "~> 2.33"
}

# ------------------------------------------------------------------------------
# DEPLOY THE WEBSERVER-CLUSTER MODULE
# ------------------------------------------------------------------------------

module "webserver_cluster" {
  #
  # this is being used if you work alone and know
  # where to locate your module
  #
  # source = "../../../../../modules/services/web"

  # Using a module with 'git' repo URL.
  # this is the best way to locate the module
  # especially if you work in a team and everyone in the
  # team must use consistent configuration
  # REMARKS:
  # If you plan to always upgrade the module based on 
  # the original repo, do not include the tag
  # But if you want to use a specific version of the 
  # module, use the release tag (the repo owner must
  # tag it before).
  #
  # source = "git::https://gitlab.com/iharijono/terra_modules.git//services/web?ref=v0.0.1"
  source = "git::https://gitlab.com/iharijono/terra_modules.git//services/web"

  mod_cluster_name  = "web-stage"
  mod_instance_type = "t2.micro"
  mod_min_size      = 2
  mod_max_size      = 2
}
