# *********************************************************************************************************************
# Owner         :   Indra G. Harijono
# Created       :   Fri, 10/18/2019
# 
# Require       :   Terraform 0.12 or higher
#                   AWS Account: IAM User with access key Id and Key
#                                use:
#                                   AWS_ACCESS_KEY_ID=AKIA3HDCOW2D3HFGSLPG
#                                   AWS_SECRET_ACCESS_KEY=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
# 
# Related to    : main.tf
# Description   : all output variables for the main file of the prod environment
# 
# *********************************************************************************************************************
  
output "clb_dns_name" {
  value       = module.webserver_cluster.mod_clb_dns_name
  description = "The domain name of the load balancer"
}