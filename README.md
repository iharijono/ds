# A Terraform based Code for the Data Syndication Project

This repo contains the terraform codes for the Data Syndication (DS) Project. Especially it contains versioned terraform modules that are being used for 'live' configuration for setting up the infrastructure(s) in DS Project.    

The 'modules' avoid having to copy and paste code for the same app deployed in multiple environments (for example: such as live/services/web/stage, live/services/web/prod)    

## Quick start

**Note**: These 'live' examples deploy resources into your AWS account. It is not my responsibility if you are charged money for this.   


1. Install [Terraform](https://www.terraform.io/).
1. Set your AWS credentials as the environment variables `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`. 
1. `cd` into 'live/services' directory. There are a few live examples there, choose one of them and `cd` into that directory (for example: "web/stage" will set up the infrastructure for web clustered service in staging environment).
```
$ cd live/services/web/stage
```
1. Run `terraform init`.
```
$ terraform init
```
1. Run `terraform plan` first, then `terraform apply`.
```
$ terraform plan
# read the output carefully, if that is what you
# want, run 'terraform apply'
$ terraform apply
...
...
Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

module.webserver_cluster.aws_security_group.mod_asg_elb: Creating...
module.webserver_cluster.aws_security_group.mod_asg_ec2: Creating...
module.webserver_cluster.aws_security_group.mod_asg_ec2: Creation complete after 3s [id=sg-0b6d46572cd8639c0]
module.webserver_cluster.aws_launch_configuration.mod_lauch_configuration: Creating...
module.webserver_cluster.aws_security_group.mod_asg_elb: Creation complete after 3s [id=sg-08d9d00066fba4e17]
module.webserver_cluster.aws_elb.mod_elb: Creating...
module.webserver_cluster.aws_launch_configuration.mod_lauch_configuration: Creation complete after 1s [id=terraform-20191021042611498600000001]
module.webserver_cluster.aws_elb.mod_elb: Creation complete after 6s [id=web-stage]
module.webserver_cluster.aws_autoscaling_group.mod_auto_scaling_group: Creating...
module.webserver_cluster.aws_autoscaling_group.mod_auto_scaling_group: Still creating... [10s elapsed]
module.webserver_cluster.aws_autoscaling_group.mod_auto_scaling_group: Still creating... [20s elapsed]
module.webserver_cluster.aws_autoscaling_group.mod_auto_scaling_group: Still creating... [30s elapsed]
module.webserver_cluster.aws_autoscaling_group.mod_auto_scaling_group: Still creating... [40s elapsed]
module.webserver_cluster.aws_autoscaling_group.mod_auto_scaling_group: Still creating... [50s elapsed]
module.webserver_cluster.aws_autoscaling_group.mod_auto_scaling_group: Still creating... [1m0s elapsed]
module.webserver_cluster.aws_autoscaling_group.mod_auto_scaling_group: Still creating... [1m10s elapsed]
module.webserver_cluster.aws_autoscaling_group.mod_auto_scaling_group: Still creating... [1m20s elapsed]
module.webserver_cluster.aws_autoscaling_group.mod_auto_scaling_group: Still creating... [1m30s elapsed]
module.webserver_cluster.aws_autoscaling_group.mod_auto_scaling_group: Still creating... [1m40s elapsed]
module.webserver_cluster.aws_autoscaling_group.mod_auto_scaling_group: Still creating... [1m50s elapsed]
module.webserver_cluster.aws_autoscaling_group.mod_auto_scaling_group: Still creating... [2m0s elapsed]
module.webserver_cluster.aws_autoscaling_group.mod_auto_scaling_group: Still creating... [2m10s elapsed]
module.webserver_cluster.aws_autoscaling_group.mod_auto_scaling_group: Still creating... [2m20s elapsed]
module.webserver_cluster.aws_autoscaling_group.mod_auto_scaling_group: Still creating... [2m30s elapsed]
module.webserver_cluster.aws_autoscaling_group.mod_auto_scaling_group: Still creating... [2m40s elapsed]
module.webserver_cluster.aws_autoscaling_group.mod_auto_scaling_group: Creation complete after 2m45s [id=tf-asg-20191021042618057300000002]

Apply complete! Resources: 5 added, 0 changed, 0 destroyed.

Outputs:

clb_dns_name = web-stage-1245657293.us-east-2.elb.amazonaws.com
```
1. After it's done deploying, the code will output URLs or IPs you can try out with "http://<EC2_INSTANCE_PUBLIC_IP>:8080".
```
$ curl http://web-stage-1245657293.us-east-2.elb.amazonaws.com:80
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    13  100    13    0     0     13      0  0:00:01 --:--:--  0:00:01    48
Hello, World
```
1. To clean up and delete all resources after you're done, run `terraform destroy`.
```
$ terraform destroy
```

## Deep Dive
A more lengthy explanation of the contents of the repo. I also take the effort to furnish the codes with the comments as complete as practical. Do read it in the source codes.

### Conventions

1. Folder structure options and naming conventions for terraform Data Syndication project
    
A typical top-level directory layout

    .
    ├── docs                            # Documentation files
    ├── live                            # Main configurations for different live environments/infrastructures (alternatively `target`)
        ├── services                    # same structure here as under "module"
            ├── elastic_search          # Main configurations for ES  
                ├── prototype           # ES prototype
                    ├── main.tf         # prototype terraform for ES
                    ├── outputs.tf      # (optional) outputs for ES
                    ├── variables.tf    # (optional) variables for ES
                ├── prod                # ES production 
                    ├── main.tf         # prod terraform for ES
                    ├── outputs.tf      
                    ├── variables.tf 
                ├── stage               # ES staging  
            ├── web                     # Main configurations for web  
                ├── prototype           # web prototype
                ├── prod                # web production 
                ├── stage               # web staging     
   ├── modules                          # Reusable modules
        ├── services                    # Reusable modules for services
            ├── elastic_search          # Reusable modules for ES
                ├── main.tf             # main terraform module for ES
                ├── outputs.tf          # outputs terraform module for ES
                ├── variables.tf        # all variables terraform module for ES
            ├── web                     # Reusable modules for web 
                ├── main.tf             
                ├── outputs.tf          
                ├── variables.tf        
    ├── tests                           # Automated tests (if available)
    ├── tools                           # Tools and utilities (if available)
    ├── LICENSE
    └── README.md    
    
1. Name, ID, variables (generic and input) and output variables will use "_" (underscore) if it contains more than one word. 
1. All terraform  variables, name or ID used in the module will be prefix with "mod". Otherwise, there is no specific prefix used. Examples:   
```
# modules
data "aws_availability_zones" "mod_all" {...}
resource "aws_autoscaling_group" "mod_example" {...}
variable "mod_server_port" {...}
output "mod_my_output" {...}

# 'live'
module "my_module" {...}
data "aws_availability_zones" "all" {...}
resource "aws_autoscaling_group" "example" {...}
variable "server_port" {...}
output "my_output" {...}

```     

## License
Please see [LICENSE.txt](/LICENSE.txt) for details on how the code in this repo is licensed.